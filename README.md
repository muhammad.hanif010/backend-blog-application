# Backend Blog Application
## Attribution
This repository was adapted from [backend article list for Magang MBKM](https://github.com/goodhel/be-todolist) by [Yudhi Kusuma](https://github.com/goodhel)

## Description
This is a repository for a backend blog application using Express.js and MySQL that can:
- Create users.
- The users can create articles.
- The users can comment on the articles.
- The articles can be edited and deleted by the users.

It can also handle CORS, JWT authentication, and be deployed with PM2 on local machine.

## Installation
1. Clone this repository.
2. Install the dependencies.
```bash
npm install
```
3. Create a database in MySQL with the name of `article_list`.
4. Run the [query.sql](query.sql) file to create the tables.
5. Run the server.
```bash
npm run dev
```
6. The server will run [localhost:5001](localhost:5001).



## Postman Documentation
[Postman Documentation](https://documenter.getpostman.com/view/23134106/VUxVrjWq)

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/23134106-68b6e96c-9484-4403-9add-b12fb7b4d401?action=collection%2Ffork&collection-url=entityId%3D23134106-68b6e96c-9484-4403-9add-b12fb7b4d401%26entityType%3Dcollection%26workspaceId%3D051eb2e3-e1d4-44ae-8367-67d52855d9ab)

## PM2 Deployment on Local Machine Screenshot
<img src="pm2-deployment-screenshot.png" alt="A screenshot showing a successful deployment of PM2 on the local machine">