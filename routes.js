const AuthController = require("./controllers/AuthController");
const CommentController = require("./controllers/CommentController");
const ArticleController = require("./controllers/ArticleController");
const UserController = require("./controllers/UserControlller");

// Define url API in Here
const _routes = [
    ['', AuthController],
    ['/articles', ArticleController],
    ['/user', UserController],
    ['/comment', CommentController],
]

// http://localhost:5001/api/articles

const routes = (app) => {
    _routes.forEach((route) => {
        const [ url, controller ] = route
        app.use(`/api${url}`, controller)
    })
}

module.exports = routes