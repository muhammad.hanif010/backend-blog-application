const { Router } = require('express')
const m$article = require('../modules/article.module')
const response = require('../helpers/response')
const userSession = require('../helpers/middleware')

const ArticleController = Router()

/**
 * List Article
 */
ArticleController.get('/', userSession, async (req, res, next) => {
    const list = await m$article.listArticle(req.query)

    response.sendResponse(res, list)
})

/**
 * Detail Article
 */
ArticleController.get('/detail', userSession, async (req, res, next) => {
    // req.query
    // http://localhost:5001/api/articles/detail?id=1
    const detail = await m$article.detailArticle(req.query.id)

    response.sendResponse(res, detail)
})

/**
 * Add Article
 * @param {string} title
 * @param {string} description
 */
ArticleController.post('/', userSession, async (req, res, next) => {
    // req.body req.params req.query
    const add = await m$article.addArticle(req.body)

    response.sendResponse(res, add)
})

/**
 * Edit Article
 * @param {number} id
 * @param {string} title
 * @param {string} description
 */
ArticleController.put('/', userSession, async (req, res, next) => {
    const edit = await m$article.editArticle(req.body)

    response.sendResponse(res, edit)
})

/**
 * Delete Article
 * @param {number} id
 */
ArticleController.delete('/:id', userSession, async (req, res, next) => {
    const del = await m$article.deleteArticle(req.params.id)

    response.sendResponse(res, del)
})

module.exports = ArticleController
